#!/bin/bash
set -euo pipefail

if [ "${#@}" -gt 0 ]; then
    python3 -m pip install ${PIP_USER+--user} "$@"
fi
[ -f .env.pip_package_overrides ] && . .env.pip_package_overrides
for override in "${!pip_package_override_@}"; do
    if [ -n "${!override:-}" ]; then
        echo "Found override: ${!override}"
        python3 -m pip install ${PIP_USER+--user} --upgrade "${!override}"
    fi
done

exit 0
