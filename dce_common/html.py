"""Some helpers for HTML output."""

import os


def hello_world(name: str) -> str:
    """Output a customized greeting."""
    greeting = os.environ.get('DCE_GREETING', 'hello world')
    return f'<p>{greeting.capitalize()}, {name}!</p>'


def goodbye(name: str) -> str:
    """Output a customized farewell."""
    farewell = os.environ.get('DCE_FAREWELL', 'goodbye')
    return f'<p>{farewell.capitalize()}, {name}!</p>'
