"""Misc utility functions."""

import os


def is_production() -> bool:
    """Check whether DCE_ENVIRONMENT == production."""
    return os.getenv('DCE_ENVIRONMENT') == 'production'


def deployment_environment() -> str:
    """Return the deployment environment, default to staging."""
    return os.getenv('DCE_ENVIRONMENT', 'staging')


def sentry_init(sentry_sdk, **kwargs):
    """
    Initialize sentry and set the correct environment.

    Optionally, kwarg parameters are added to the sentry_sdk.init call.
    """
    params = {
        'ca_certs': os.getenv('REQUESTS_CA_BUNDLE'),
        'environment': deployment_environment(),
    }
    params.update(kwargs)

    sentry_sdk.init(**params)
