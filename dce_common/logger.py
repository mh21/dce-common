"""Common logging functions."""
import logging
import os
import sys
import threading

STREAM = sys.stderr
FORMAT = '%(asctime)s.%(msecs)03d - [%(levelname)s] - %(name)s - %(message)s'
FORMAT_DATE = '%Y-%m-%dT%H:%M:%S'
LOCK = threading.Lock()
DCE_HANDLER = False


def get_logger(logger_name: str) -> logging.Logger:
    """Return DCE logger or descendant."""
    # Make sure adding handler is thread safe since get_logger might not be
    # called from main
    # https://docs.python.org/3/library/logging.html#logging.basicConfig
    with LOCK:
        global DCE_HANDLER  # pylint: disable=global-statement
        dce = DCE_HANDLER
        DCE_HANDLER = True

    if not dce:
        # Add STREAM handler to root logger.
        root_logger = logging.getLogger()
        handler = logging.StreamHandler(STREAM)
        root_logger.addHandler(handler)
        formatter = logging.Formatter(fmt=FORMAT, datefmt=FORMAT_DATE)
        handler.setFormatter(formatter)

        # Set dce loglevel to DCE_LOGGING_LEVEL.
        dce = logging.getLogger('dce')
        dce.setLevel(os.environ.get('DCE_LOGGING_LEVEL', 'WARNING'))

    if not (logger_name == 'dce' or logger_name.startswith('dce.')):
        logger_name = 'dce.' + logger_name

    return logging.getLogger(logger_name)
