#!/bin/bash

set -euo pipefail

function main() {
    echo "Started: $(date -Iseconds)"

    echo "PID shell: $$"

    if [ -v DCE_START_FLASK ]; then
        if [[ "${DCE_ENVIRONMENT:-}" = "production" ]] ; then
            gunicorn --bind 0.0.0.0:5000 --workers "${DCE_WEB_WORKERS:-2}" "${DCE_START_FLASK}:app" &
        else
            FLASK_APP=${DCE_START_FLASK} FLASK_ENV=development flask run --host 0.0.0.0 --port 5000 &
        fi
        echo "PID flask: $!"
    fi

    # shellcheck disable=SC2153
    for PYTHON_VAR in "${!DCE_START_PYTHON_@}"; do
        IFS=' ' read -r -a ARGS <<< "${!PYTHON_VAR}"
        python3 -m "${ARGS[@]}" &
        echo "PID $PYTHON_VAR: $!"
    done

    # If even one process exits, kill all spawned processes
    echo "Waiting for first process to exit"
    wait -n || true

    echo "Killing process group: -$$"
    kill -- -$$
}

# make getpwuid_r happy
if [ -w '/etc/passwd' ] && ! id -nu > /dev/null 2>&1; then
    echo "dce:x:$(id -u):$(id -g):,,,:${HOME}:/bin/bash" >> /etc/passwd;
fi

# For ^C and SIGTERM for the container, kill the whole process group
trap 'trap - SIGINT SIGTERM && kill -- -$$' SIGINT SIGTERM

main 2>&1 | tee -a "/logs/${HOSTNAME}.log"
